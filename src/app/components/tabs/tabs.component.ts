import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

  form!: FormGroup;
  etiqueta!: string;

  constructor(private fb: FormBuilder) { 
    this.crearForm();
  }

  ngOnInit(): void {
    console.log(this.etiqueta);
    
  }

  crearForm(): void{
    this.form = this.fb.group({
      name: [''],
    })
  }

  cambiarValor(): void{
    console.log(this.form);
    this.etiqueta = this.form.value.name
    
  }

  guardarForm1(): void{

  }

  almacenar(): void {
    
  }

}
