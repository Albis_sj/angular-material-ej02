import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormDosComponent } from './components/form-dos/form-dos.component';
import { FormUnoComponent } from './components/form-uno/form-uno.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
